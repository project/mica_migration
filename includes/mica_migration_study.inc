<?php
/**
 * @file
 * Mica_export_study.inc.
 */

/**
 * Construct of the Dto studies.
 *
 * @param object $node
 *   The study node to export.
 * @param array $context
 *   An array collecting values var during the batch operation.
 */
function _mica_migration_study_dto($node, array &$context) {
  $dto_util = new MicaMigrationDtoHelpers();
  $wrapper = entity_metadata_wrapper('node', $node);
  $wrapper_id = $wrapper->uuid->value();
  $study_dto = _mica_migration_construct_study_dto($dto_util, $wrapper, $context);
  $codec = new \DrSlump\Protobuf\Codec\Json();
  // Create study-dto.
  $study_dto->setId($wrapper_id);
  $data = $study_dto->serialize($codec);

  $context['results']['study']['uuid'] = $wrapper_id;
  $context['results']['study']['title_study'] = str_replace('/', '_', $wrapper->title->value());
  $context['results']['study']['study_json'] = $data;
}

/**
 * Convert study fields to Dto object studies.
 *
 * @param object $dto_util
 *   The instantiated Dto util class MicaMigrationDtoHelpers.
 * @param object $wrapper
 *   The Entity wrapper node to export (Study, network, ...).
 * @param array $context
 *   An array collecting values var during the batch operation.
 *
 * @return \obiba\mica\StudyDto
 *   The Dto Object.
 */
function _mica_migration_construct_study_dto($dto_util, $wrapper, array &$context) {

  $study_dto = new obiba\mica\StudyDto();
  $dto_util->addTranslatedField($study_dto, 'addName', $wrapper, 'title_field');

  $study_acronym = $wrapper->field_acroym->value();
  if (!empty($study_acronym)) {
    $dto_util->addTranslatedField($study_dto, 'addAcronym', $wrapper, 'field_acroym');
  }

  foreach ($wrapper->field_investigators->getIterator() as $investigator_wrapper) {
    if (!empty($investigator_wrapper)) {
      $investigator = _mica_migration_contact_dto($dto_util, $investigator_wrapper);
      $study_dto->addInvestigators($investigator);
    }
  }

  $dto_util->addTranslatedField($study_dto, 'addObjectives', $wrapper, 'body', 'value');

  $url_website = $wrapper->field_website->value();
  if (!empty($url_website)) {
    $study_dto->setWebsite($url_website['url']);
  };

  $opal_study = $wrapper->mica_opal->value();
  if (!empty($opal_study)) {
    $study_dto->setOpal($opal_study['url']);
  };
  foreach ($wrapper->field_contacts_ref->getIterator() as $contact_wrapper) {
    $title_contact = $contact_wrapper->title->value();
    if (!empty($title_contact)) {
      $contact = _mica_migration_contact_dto($dto_util, $contact_wrapper);
      $study_dto->addContacts($contact);
    }
  }

  $authorization_spec_dto = new \obiba\mica\AuthorizationDto();
  $authorization_spec_dto->setAuthorized($wrapper->field_authorization_specific->value());
  $authorization_spec_dto->setAuthorizer($wrapper->field_authorising_person_name->value());

  $authorization_spec_dto->setDate(date("c", $wrapper->field_authorising_date->value()));
  $study_dto->setSpecificAuthorization($authorization_spec_dto);

  $authorization_maelstrom_dto = new \obiba\mica\AuthorizationDto();
  $authorization_maelstrom_dto->setAuthorized($wrapper->field_authorization_maelstrom->value());
  $authorization_maelstrom_dto->setAuthorizer($wrapper->field_authorising_person_name_m->value());

  $authorization_maelstrom_dto->setDate(date("c", $wrapper->field_authorising_date_m->value()));
  $study_dto->setMaelstromAuthorization($authorization_maelstrom_dto);

  $study_methods_dto = new \obiba\mica\StudyDto\StudyMethodsDto();
  foreach ($wrapper->field_design->getIterator() as $design) {
    $study_methods_dto->addDesigns($design->value());
    if ($design->value() == 'other') {
      $design_other_sp = $wrapper->field_design_other_sp->value();
      // dpm( $wrapper->language('en')->field_design_other_sp->value());
      if (!empty($design_other_sp)) {
        $dto_util->addTranslatedField($study_methods_dto, 'addOtherDesign', $wrapper,
          'field_design_other_sp', 'value');
      }
    }
  }
  $info_design_follow_up = $wrapper->field_info_design_follow_up->value();

  if (!empty($info_design_follow_up)) {
    $dto_util->addTranslatedField($study_methods_dto, 'addFollowUpInfo', $wrapper,
      'field_info_design_follow_up', 'value');
  }

  foreach ($wrapper->field_recruitment->getIterator() as $target) {
    $study_methods_dto->addRecruitments($target->value());
    if ($target->value() == 'other') {
      $recruitment_other_sp = $wrapper->field_recruitment_other_sp->value();
      if (!empty($recruitment_other_sp)) {
        $dto_util->addTranslatedField($study_methods_dto, 'addOtherRecruitment', $wrapper,
          'field_recruitment_other_sp');
      }
    }
  }

  $recruitment_supp_info = $wrapper->field_recruitment_supp_info->value();
  if (!empty($recruitment_supp_info)) {
    $dto_util->addTranslatedField($study_methods_dto, 'addInfo', $wrapper,
      'field_recruitment_supp_info', 'value');
  }

  $study_dto->setMethods($study_methods_dto);

  $participant_sample['number'] = $wrapper->field_target_number_participants->value();
  $participant_sample['noLimit'] = $wrapper->field_no_limits_participants->value();
  $biosample['number'] = $wrapper->field_target_number_biosamples->value();
  $biosample['noLimit'] = $wrapper->field_no_limits_samples->value();
  $study_dto->setNumberOfParticipants(
    mica_migration_set_number_of_participants($wrapper, 'field_target_nb_supp_info',
      $participant_sample,
      $biosample));

  $study_dto->setStartYear($wrapper->field_study_start_year->value());
  $study_dto->setEndYear($wrapper->field_study_end_year->value());

  if ($wrapper->field_access_data->value() == 1) {
    $study_dto->addAccess('data');
  }
  if ($wrapper->field_access_biosamples->value() == 1) {
    $study_dto->addAccess('bio_samples');
  }
  if ($wrapper->field_access_other->value() == 1) {
    $study_dto->addAccess('other');
    $access_other_info = $wrapper->field_access_other_sp->value();
    if (!empty($access_other_info)) {
      $dto_util->addTranslatedField($study_dto, 'addOtherAccess', $wrapper,
        'field_access_other_sp', 'value');
    }
  }

  $study_dto->setMarkerPaper($wrapper->field_marker_paper->value());
  $study_dto->setPubmedId($wrapper->field_pubmedid->value());

  $supl_info = $wrapper->field_supp_infos->value();

  if (!empty($supl_info)) {
    $dto_util->addTranslatedField($study_dto, 'addInfo', $wrapper,
      'field_supp_infos', 'value');
  }

  foreach ($wrapper->field_study_populations->getIterator() as $population_wrapper) {
    if (!empty($population_wrapper)) {
      $population = _mica_migration_population_dto($dto_util, $population_wrapper, $context);
      $study_dto->addPopulations($population);
    }
  }

  $attachments_files = array();

  $logo = $wrapper->field_logo->value();
  if (!empty($logo['filename'])) {
    $attachment_logo_dto = $dto_util->attachmentConstruct($logo, 'logo');
    $attachments_files[] = array(
      'uuid' => $logo['uuid'],
      'filename' => $logo['filename'],
      'uri' => $logo['uri'],
    );
    $study_dto->setLogo($attachment_logo_dto);
  }

  foreach ($wrapper->field_documents->value() as $attachment) {
    if (!empty($attachment['filename'])) {
      $attachment_dto = $dto_util->attachmentConstruct($attachment, 'document');
      $attachments_files[] = array(
        'uuid' => $attachment['uuid'],
        'filename' => $attachment['filename'],
        'uri' => $attachment['uri'],
      );
      $study_dto->addAttachments($attachment_dto);
    }
  }

  if (!empty($attachments_files)) {
    $context['results']['study']['attachments'] = $attachments_files;
  }
  return $study_dto;
}

/**
 * Set Nbr of participants field Dto.
 *
 * @param object $wrapper
 *   The Entity wrapper node to export (Study, network, ...).
 * @param string $field
 *   The field name.
 * @param array $participant
 *   The array of participant field.
 * @param array $sample
 *   The array of sample field.
 *
 * @return \obiba\mica\StudyDto\NumberOfParticipantsDto
 *   The Dto Object.
 */
function mica_migration_set_number_of_participants($wrapper, $field, array $participant = NULL, array $sample = NULL) {
  $participant_dto = new \obiba\mica\StudyDto\NumberOfParticipantsDto();
  $dto_util = new MicaMigrationDtoHelpers();
  $participant_dto->setParticipant(mica_migration_set_target_number_dto($participant));
  $participant_dto->setSample(mica_migration_set_target_number_dto($sample));
  $value_field = $wrapper->{$field}->value();
  !empty($value_field) ? $dto_util->addTranslatedField($participant_dto, 'addInfo', $wrapper, $field, 'value') : NULL;
  return $participant_dto;
}

/**
 * Set target Nbr field Dto.
 *
 * @param array $target
 *   Array values of an field (Participant, sample).
 *
 * @return \obiba\mica\TargetNumberDto
 *   The Dto Object
 */
function mica_migration_set_target_number_dto(array $target) {
  $target_number = new \obiba\mica\TargetNumberDto();
  $target_number->setNumber($target['number']);
  $target_number->setNoLimit($target['noLimit']);
  return $target_number;
}
